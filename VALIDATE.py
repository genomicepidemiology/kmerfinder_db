#!/usr/bin/python3
''' Validate Database for KmerFinder-3.+'''

import sys, os, re

## ADD ARGUMENT FOR SPECIFIC DATABASE
if len(sys.argv) == 2:
    #db_path = sys.argv[1]
    sys.stderr.write("Error: Please specify a database path and database name. With 'all' all databases will be validated, otherwise provide specific database name, such as 'bacteria'.\n")
    sys.exit(2)
elif len(sys.argv) == 3:
    db_path = sys.argv[1]
    db_name = sys.argv[2]
else:
    db_path = os.path.dirname(os.path.realpath(__file__))
    db_name = 'all'
    print("Warning: you have not specified database path or name. In this case all databases will be validated and it is assumed that the databases are installed and saved in your current working directory: ",db_path)
    print("In case you get an error please specify both database path and name.")

# Check existence of config file
db_config_file = '{}/config'.format(db_path)
if not os.path.exists(db_config_file):
    sys.exit("Error: The database config file could not be found!")

valid_dbs = ["all","archaea", "bacteria", "protozoa", "fungi", "typestrain", "viral"]

# Check if db name is valid
if db_name not in valid_dbs:
    sys.exit("Error: The database name is not valid. Please provide a valid database name, among the following: all, archaea, bacteria, fungi, plasmids, protozoa, typestrains")

def check_empty_file(file):
    if os.path.getsize(file) > 0:
        pass
    else:
        sys.exit("Error: The file %s is corrupted. Pleased download the latest"
                 "version of the database (with UPDATE.sh), or if it is the "
                 "latest update contact the curator." % (file))
dbs = []
extensions = []
with open(db_config_file) as f:
    for l in f:
        l = l.strip()
        if l == '': continue
        if l[0] == '#':
            if 'important files are:' in l:
                # Check for all databases, or specific one
                if db_name != 'all':
                    files = ["%s/%s%s"%(db_path, db_name,s.strip())
                             for s in l.split('are:')[-1].split(',')]
                else:
                    files = []
                    for db in valid_dbs[1:]:
                        files_db = ["%s/%s/%s%s"%(db_path, db, db, s.strip())
                                 for s in l.split('are:')[-1].split(',')]
                        files.extend(files_db)
            if 'extensions:' in l:
                extensions = [s.strip() for s in l.split('extensions:')[-1].split(',')]
            continue
        tmp = l.split('\t')
        if len(tmp) != 3:
            sys.exit(("Error: Invalid line in the database config file!\n"
                   "A proper entry requires 3 tab separated columns!\n%s")%(l))
        db_prefix = tmp[0].strip()
        name = tmp[1].split('#')[0]
        description = tmp[2]
        # Check if all db files are present
        if db_name != 'all':
            if l.split(".")[0] != db_name:
                pass
            else:
                for ext in extensions:
                    if ext != "b":
                        path = "%s/%s/%s.%s"%(db_path, db_prefix.split(".")[0], db_prefix, ext)
                        if not os.path.exists(path):
                            sys.exit(("Error: The database file (%s) could not be found!")%(
                            path))
                        check_empty_file(path)
                        dbs.append((name, db_prefix))
        else:
            for db in valid_dbs[1:]:
                for ext in extensions:
                    if ext != "b":
                        path = "%s/%s/%s.%s"%(db_path, db_prefix.split(".")[0], db_prefix, ext)
                        if not os.path.exists(path):
                            sys.exit(("Error: The database file (%s) could not be found!")%(
                            path))
                        check_empty_file(path)
                        dbs.append((name, db_prefix))


if len(dbs) == 0:
   sys.exit("Error: No databases were found in the database config file!")
else:
   print("Validation passed. Database is valid.")
