#!/bin/bash

# Choose which database to download: two options--> 1. the organism name 2. all available databases
# Choose which database verison to download: two options--> 1. latest 2. specific date (as in FTP site YYYMMDD)
# If no option is given for tag, then the script automatically downloads the latest
db_dir=$1
db_name=$2
tag=$3
#DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

### Check arguments
## Check first argument -directory to save database(s)
if [ -z "$1" ]; then
   echo "Please specify directory to save downloaded database(s)."
   exit
fi

if [ -d "$1" ]; then
    echo ""
else
    echo "The provided argument is not a directory. Please provide a valid directory as a first argument. See specifications of INSTALL.sh program."
    exit
fi

# Check second argument, database name
if [ -z "$2" ]; then
   echo "You have not specified which database to download. If you want to download all available databases, please specify with 'all'. If you want a specific database please specify with e.g. 'bacteria' or 'protozoa'."
   exit
fi

## Check if database name is valid
if [[ "$db_name" =~ ^(all|archaea|bacteria|fungi|plasmids|protozoa|typestrain|viral)$ ]]; then
    echo "Downloading database(s) for $db_name"
else
    echo "Input database name is not valid. You can select to download using the following options: all, archaea, bacteria, fungi, plasmids, protozoa, typestrains"
    exit
fi

## Check tag argument
if [ -z "$3" ]; then
   echo "Date tag was not specified. Downloading latest updated database automatically."
   tag="latest"
fi


## Check if tag argument exists
url="ftp://ftp.cbs.dtu.dk/public/CGE/databases/KmerFinder/version/$tag/"
if curl --output /dev/null --silent --head --fail "$url"; then
  echo "Downloading $db_name database(s) for date tag: $tag"
else
  echo "Input date tag is not valid. There is no database available for this date. Please check the ftp site ftp://ftp.cbs.dtu.dk/public/CGE/databases/KmerFinder/version/ for valid updates tags of the databases."
fi

## Download the newest version of the KmerFinder databases
if [ "$db_name" == "all" ]; then
	wget ftp://ftp.cbs.dtu.dk/public/CGE/databases/KmerFinder/version/$tag/* -P $db_dir
    ls $db_dir/*.tar.gz |xargs -n1 tar -C $db_dir -xzf
else
	wget ftp://ftp.cbs.dtu.dk/public/CGE/databases/KmerFinder/version/$tag/$db_name* -P $db_dir
    wget ftp://ftp.cbs.dtu.dk/public/CGE/databases/KmerFinder/version/$tag/config -P $db_dir
    tar -xzf $db_dir/$db_name.tar.gz -C $db_dir
fi

# Delete tarball, keep md5
rm $db_dir/*.tar.gz

# # # Optional choice to fix permission problems
# # echo "Do you want to fix permissions, yes/y or no/n, followed by [ENTER]:"
# # read fix
# # if [[ $fix = "yes" ]] || [[ $fix = "y" ]]; then
   # # echo "enter group name followed by [ENTER]:"
   # # read gn
   # # if [ ! -z "$gn" ]; then
      # # chgrp -R $gn $path
   # # fi
   # # find $path \( -name .git \) -prune -printf '' -o -type d -print -exec chmod 775 {} \;
   # # find $path \( -name .git \) -prune -printf '' -o -type f -print -exec chmod 664 {} \;
# # fi
