KmerFinder Repository
===================
This repository contains instructions and scripts for installing the KmerFinder
database. In addition, the \*.content.tsv files keeps track of the content in
the databses. This allows us to track the changes in the databases without
tracking the big binary files.

### What is this repository for? ###
The databases stored in this repository and the files stored on our FTP site,
are needed by the program KmerFinder
(https://bitbucket.org/genomicepidemiology/kmer-finder)

### Contents ###
* A config file specifying what files are used for which databases
* Lists of genomes used in the creation of the KmerFinder databases
* 2 Bash scripts to download the large binary files from a FTP site
* 1 Python script to validate that all the needed files are present

This database repository contains several database which are described in the
config file.
The individual databasee were constructed from a NCBI download of all available
whole genomes and draft genomes, and has following been checked and fixed for
bad files, genomes and annotations. Some databases have also been downloaded
from other sources. See the source section for details.

### How do I get set up? ###
* Clone this repository
* Run the installation script "INSTALL". This will download all the binary files
* Verify the database is installed correctly by running "VALIDATE"
* If you later need to update the databases, just run the script 'UPDATE'

### Sources ###
https://www.ncbi.nlm.nih.gov/bioproject
https://www.ncbi.nlm.nih.gov/assemblies
https://bitbucket.org/genomicepidemiology/resfinder_db

### Who do I talk to? ###
To get in contact with us, please contact cgehelp@cbs.dtu.dk
