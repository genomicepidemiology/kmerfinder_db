KMERFINDER DATABASES
===================

This bitbucket page is only kept for legacy purposes and no longer relevant. The KmerFinder database can be downloaded directly and ready to use as a Tar archive [here](https://cge.food.dtu.dk/services/KmerFinder/etc/kmerfinder_db.tar.gz). 

________________


This project documents the installation, validation and update of KmerFinder databases.
The databases stored on our FTP site, are needed by the program KmerFinder (https://bitbucket.org/genomicepidemiology/kmerfinder/src)

Documentation
=============

## Content of the repository

1. INSTALL.sh      - the program for installing all databases or one specific database
2. VALIDATE.py     - the program for validating all databases or one specific database
3. UPDATE.sh       - the program for updating all databases or one specific database
4. README.md  

 
## Installation - Clone repository
```bash
# Go to wanted location for KmerFinder db 
cd /path/to/some/dir
# Clone and enter the KmerFinder db directory
git clone https://bitbucket.org/genomicepidemiology/kmerfinder_db.git
cd kmerfinder_db
```

## Download and install kmerfinder database(s)

The INSTALL program allows you to install either all available databases (archaea, bacteria, fungi, plasmids, protozoa, typestrains) or to download one specific database.
There is also an extra option to download database(s) from a specific date tag (update). If there is no date tag specified, database(s) will be downloaded automatically 
from the most current update. The INSTALL program downloads the binary files of the database(s).

INPUTS:

1. path/to/some/dir                                     - where to save the database
2. all OR specific taxonomic group, e.g. bacteria       - which database to download. 'all' is for downloading all databases. To download one database, define with 'bacteria', 'fungi', 'plasmids' etc.
3. version tag: none, latest or date as YYYYMMDD        - the version or date tag from where to download the database(s). By not providing an argument or giving 'latest' as an argument, database(s) are automatically downloaded from the latest update.
                                                          Otherwise you can provide a date in the format of YYYYMMDD to download from a specific date.

```bash
# Go to the directory where you want to store the KmerFinder database
cd /path/to/some/dir
KmerFinder_DB=$(pwd)
# Go back to the directory where the INSTALL.sh is (repository)
# Below we provide alternative example commands to download the database(s) you want.
#1. Install all KmerFinder databases from latest update
bash INSTALL.sh $KmerFinder_DB all 
#2. Install all KmerFinder databases from specific date - update - you can find available updates in our FTP site.
bash INSTALL.sh $KmerFinder_DB all 20180713
#3. Install only one KmerFinder database from latest update, e.g. bacteria
bash INSTALL.sh $KmerFinder_DB bacteria latest
#4. Install only the bacteria KmerFinder database from specific date - update - you can find available updates in our FTP site.
bash INSTALL.sh $KmerFinder_DB bacteria 20180713
```

## Validate the kmerfinder database(s)

The VALIDATE program allows you to validate whether the dowloaded database(s) are/is installed correctly by looking at the config file, which describes all the
available databases. You can validate either all available databases (archaea, bacteria, fungi, plasmids, protozoa, typestrains) or one specific database, if you do not have
all the databases installed.

INPUTS:

1. path/to/some/dir                                     - where the database(s) is/are saved.
2. all OR specific taxonomic group, e.g. bacteria       - which database to validate. 'all' is for validating all databases. To validate one database, define with 'bacteria', 'fungi', 'plasmids' etc.

```bash
# Go to the directory where the KmerFinder database(s) are stored
cd /path/to/some/dir
KmerFinder_DB=$(pwd)
# Go back to the directory where the VALIDATE.py is (repository)
# Below we provide alternative example commands to download the database(s) you want.
#1. Validate all KmerFinder databases 
python3 VALIDATE.py $KmerFinder_DB all 
#2. Validate only one KmerFinder database, e.g. bacteria
python3 VALIDATE.py $KmerFinder_DB bacteria
```

## Update the kmerfinder database(s)

The UPDATE program allows you to update the database(s) you have already installed. Since out databases are frequently updated, you might want to get the latest
databases at some point. You can update either all available databases (archaea, bacteria, fungi, plasmids, protozoa, typestrains) or one specific database, 
if you do not have all the databases installed. 
The program checks if there was an update comparing the database(s) you have installed and the latest updated database(s) and only installs the new, current database(s)
if an update is found. If there is an update, it downloads the latest updated database(s) automatically. Thus, if you would like a database from
a specific update(date) you are advised to use the INSTALL program instead, that gives you this option.

INPUTS:

1. path/to/some/dir                                     - where the database(s) is/are saved.
2. all OR specific taxonomic group, e.g. bacteria       - which database to update. 'all' is for updating all databases. To update one database, define with 'bacteria', 'fungi', 'plasmids' etc.

```bash
# Go to the directory where the KmerFinder database(s) are stored
cd /path/to/some/dir
KmerFinder_DB=$(pwd)
# Go back to the directory where the UPDATE.sh is (repository)
# Below we provide alternative example commands to download the database(s) you want.
#1. Update all KmerFinder databases 
bash UPDATE.sh $KmerFinder_DB all 
#2. Update only one KmerFinder database, e.g. bacteria
bash UPDATE.sh $KmerFinder_DB bacteria
```
## Available databases
You can find all our available databases in our [FTP site](ftp://ftp.cbs.dtu.dk/public/CGE/databases/KmerFinder/version/)

***IMPORTANT NOTE:*** these scripts for installation, validation and updating of KmerFinder Databases are implemented for databases created 
after 13 July 2018 (tag: 20180713).

## Sources
https://www.ncbi.nlm.nih.gov/bioproject

https://www.ncbi.nlm.nih.gov/assemblies

## Who do I talk to?
To get in contact with us, please contact food-cgehelp@dtu.dk