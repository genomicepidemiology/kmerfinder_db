#!/bin/bash/

# This script updates downloaded databases from the CGE ftp site. It evaluates if there has been an update in chosen databases
# and if yes, it downloads the new updated database(s). To do this, md5 files are necessery.
# Important note: the script automatically checks and downloads the LATEST updated database(s), so there is not an option to choose
# specific date tag and update with this one. If you would like to get the database from a specific date, please use the INSTALL.sh script.

# INPUTS:
# 1. database directory where current database is
# 2. database name --> 'all' if you have all the databases, or specific name, e.g. 'bacteria' to update a specific database.


db_dir=$1
db_name=$2

### Check arguments
## Check first argument -directory to save database(s)
if [ -z "$1" ]; then
   echo "Please specify directory where downloaded database(s) are saved. This is the same directory where the new, updated databases will be saved. Old database(s) are overwritten."
   exit
fi

if [ -d "$1" ]; then
    echo ""
else
    echo "The provided argument is not a directory. Please provide a valid directory as a first argument. See specifications of UPDATE.sh program."
    exit
fi

# Check second argument, database name
if [ -z "$2" ]; then
   echo "Please specify which database to update. If you want to update all available databases, please specify with 'all'. If you want a specific database please specify with e.g. 'bacteria' or 'protozoa'."
   exit
fi

## Check if database name is valid
if [[ "$db_name" =~ ^(all|archaea|bacteria|fungi|plasmids|protozoa|typestrain|viral)$ ]]; then
    echo "Checking for updates in database(s) for $db_name"
else
    echo "Input database name is not valid. You can select to update using the following options: all, archaea, bacteria, fungi, plasmids, protozoa, typestrains"
    exit
fi

# Set tag to latest
tag="latest"

## Download md5 file(s), save to temporary file, compare with "old" md5 file(s)
mkdir -p $db_dir/tmp
if [ "$db_name" == "all" ]; then
    # First download the md5 files
    wget ftp://ftp.cbs.dtu.dk/public/CGE/databases/KmerFinder/version/$tag/*.md5 -P $db_dir/tmp -q

    # Then check for each of the databases if there has been an update
    for db in archaea bacteria fungi protozoa typestrain viral; do
        db_c=${db##*/}
        tax_name="${db_c%.*}"
        db_md5=$tax_name.md5
        echo "$db_md5"
        # If there is an update download updated db
        if [ "`cat $db_dir/$db_md5 | awk '{print $1}'`" != "`cat $db_dir/tmp/$db_md5 | awk '{print $1}'`" ]; then
            echo "Downloading updated database(s) for $tax_name"
            wget ftp://ftp.cbs.dtu.dk/public/CGE/databases/KmerFinder/version/$tag/$db.tar.gz -P $db_dir
            tar --overwrite-dir -xzf $db_dir/$db.tar.gz
            ## Delete tarball, change md5
            rm $db_dir/*.tar.gz
            mv $db_dir/tmp/$db_md5 $db_dir/$db_md5
            echo "Database updated!"
        else
            echo "There was no update for $tax_name database"
        fi
    done

else
    db_md5=$db_name.md5
    # First download the md5 files
	wget ftp://ftp.cbs.dtu.dk/public/CGE/databases/KmerFinder/version/$tag/$db_md5 -P $db_dir/tmp -q
    # If there is an update download updated db
    if [ "`cat $db_dir/$db_md5 | awk '{print $1}'`" != "`cat $db_dir/tmp/$db_md5 | awk '{print $1}'`" ]; then
        echo "Downloading updated database(s) for $db_name"
        wget ftp://ftp.cbs.dtu.dk/public/CGE/databases/KmerFinder/version/$tag/$db_name* -P $db_dir
#        wget ftp://ftp.cbs.dtu.dk/public/CGE/databases/KmerFinder/version/$tag/config -P $db_dir
        tar -xzf --overwrite-dir $db_dir/$db_name.tar.gz -C $db_dir
        ## Delete tarball, keep md5
        rm $db_dir/*.tar.gz
        echo "Database updated!"
    else
        echo "There was no update for $db_name database"
    fi
fi

## Cleanup tmp
rm -rf $db_dir/tmp
wget ftp://ftp.cbs.dtu.dk/public/CGE/databases/KmerFinder/version/$tag/config -O $db_dir/config
# echo "Do you want to fix permissions, yes/y or no/n, followed by [ENTER]:"
# read fix
# if [[ $fix = "yes" ]] || [[ $fix = "y" ]]; then
   # echo "enter group name followed by [ENTER]:"
   # read gn
   # if [ ! -z "$gn" ]; then
      # chgrp -R $gn $path
   # fi
   # find $path \( -name .git \) -prune -printf '' -o -type d -print -exec chmod 775 {} \;
   # find $path \( -name .git \) -prune -printf '' -o -type f -print -exec chmod 664 {} \;
# fi
